package main

import (
	"log"

)

func logEnter(s string) { log.Println("[TRACE] entering: ", s) }
func logLeave(s string) { log.Println("[TRACE] leaving: ", s) }
func logPrint(s string) { log.Println("[TRACE] ", s) }
