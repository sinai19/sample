//-------------------------------------------------------
// Description: ProxyServer
// Environment:
//
//
// kucst_2020.01.29
//-------------------------------------------------------
package main

import (
	"fmt"
	"net"

)

func main() {

	Init_ReqPool()

	fmt.Println("[INFO] The server is listening on Port 8080")
	listener, _ := net.Listen("tcp", "localhost:8080")
	defer listener.Close()

	// Listen for connections
	for {
		conn, _ := listener.Accept()

		fmt.Println("[INFO] New connection found!")
		go listenConnection(conn)
	}

}

func Init_ReqPool() {
	&gReqPool = NewRequests()
}

//-------------------------------------------------------
// Description:
// Environment: connected
// 1 step: Read the request from conn
// 2 step: call ResponseProcess with input read from request
// Sinai_2020.01.29
//-------------------------------------------------------
func AcceptRequest(conn net.Conn) {

	input := ""

	fmt.Sprintf("[INFO] %s", "Read request from the client")
	buffer := make([]byte, 2400)
	_, err := conn.Read(buffer)
	if err != nil {
		fmt.Sprintf("[ERROR] %s", err.Error())
	}

	//kucst_2020-01-30
	gReqPool.addRequest(NewRequest(conn.RemoteAddr().String, string(buffer)))

	fmt.Sprintf("[INFO] %s", "Call ResponseProcess")
	input = string(buffer)
	if "OK" != ResponseProcess(input, conn) {
		fmt.Sprintf("[ERROR] %s", "Not OK")
	}
}

//-------------------------------------------------------
// Description:
// Environment: conditions
// 1 step: check the input to decide what to do
// 2 step: do something
// 3 step: send the response back
// 4 step: return the result of sending
// Sinai_2020.01.29
//-------------------------------------------------------
func ResponseProcess(input string, conn net.Conn) string {

	buffer := make([]byte, 2400)
	_, err := conn.Read(buffer)
	response := ""
	fmt.Sprintf("[INFO] %s", "____Do something____")
	response = "ack" + input

	fmt.Sprintf("[INFO] %s", "Send the response of request back to the client")
	_, err = conn.Write([]byte(response))
	if err != nil {
		fmt.Sprintf("[ERROR] %s", err.Error())
		return "Not OK"
	}

	return "OK"
}

//-------------------------------------------------------
// Description:
// Environment: conditions
// 1 step: check the input to decide what to do
// 2 step: do something
// 3 step: send the response back
// 4 step: return the result of sending
// Sinai_2020.01.29
//-------------------------------------------------------
func listenConnection(conn net.Conn) {

	fmt.Sprintf("[INFO] %s", "Call AcceptRequest ")
	AcceptRequest(conn)

}
