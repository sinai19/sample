package main

import (
	"fmt"
	"net"
	"os"
	"time"

)

//command index 설정
const (
	setnodeport       = 10
	setnodeip         = 11
	setleadernodeaddr = 12
	createblockchain  = 13
	createwallet      = 14
	getwallet         = 15
	getbalance        = 16
	listaddresses     = 17
	reindexutxo       = 18
	charge            = 19
	send              = 20
	startnode         = 21
	getblock          = 22
)

//command 별 Payload 구조체 설정
type T_basic_Payload struct {
	cmd string `json:"cmdString`
}

type T_GetWallet_Payload struct {
	cmd  string `json:"cmdString"`
	addr string `json:"addr"`
}

type T_GetBalance_Payload struct {
	cmd  string `json:"cmdString"`
	addr string `json:"addr"`
}

type T_Charge_Payload struct {
	cmd     string `json:"cmdString"`
	to_addr string `json:"toAddr"`
	amount  string `json:"amount"`
}

type T_Send_Payload struct {
	cmd       string `json:"cmdString"`
	from_addr string `json:"fromAddr"`
	to_addr   string `json:"toAddr"`
	amount    string `json:"amount"`
}

type T_GetBlock_Payload struct {
	cmd     string `json:"cmdString"`
	blockid string `json:"blockid"`
}

func main() {

	service_port := "127.0.0.1:8080"
	client, err := net.Dial("tcp", service_port)
	checkError(err)
	defer client.Close()

	go func(c net.Conn) {

		data := make([]byte, 4096)
		for {
			n, err := c.Read(data)
			checkError(err)
			fmt.Println("[proxyClient] recv: ", string(data[:n]))
			time.Sleep(1 * time.Second)
		}
	}(client)

	go func(c net.Conn) {

		i := 10
		s := ""
		for {

			switch i {
			case 10: //setnodeport
				pk := T_basic_Payload{}
				pk.cmd = "setnodeport"
				s = fmt.Sprintf("%s", pk.cmd)

			case 11: //setnodeip
				pk := T_basic_Payload{}
				pk.cmd = "setnodeip"
				s = fmt.Sprintf("%s", pk.cmd)

			case 12: //setleadernodeaddr
				pk := T_basic_Payload{}
				pk.cmd = "setleadernodeaddr"
				s = fmt.Sprintf("%s", pk.cmd)

			case 13: //createblockchain
				pk := T_basic_Payload{}
				pk.cmd = "createblockchain"
				s = fmt.Sprintf("%s", pk.cmd)

			case 14: //createwallet
				pk := T_basic_Payload{}
				pk.cmd = "createwallet"
				s = fmt.Sprintf("%s", pk.cmd)

			case 15: //getwallet
				pk := T_GetWallet_Payload{}
				pk.cmd = "getwallet"
				pk.addr = "1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX"
				s = fmt.Sprintf("%s -address %s", pk.cmd, pk.addr)

			case 16: //getbalance
				pk := T_GetBalance_Payload{}
				pk.cmd = "getbalance"
				pk.addr = "1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX"
				s = fmt.Sprintf("%s -address %s", pk.cmd, pk.addr)

			case 17: //listaddresses
				pk := T_basic_Payload{}
				pk.cmd = "listaddresses"
				s = fmt.Sprintf("%s", pk.cmd)

			case 18: //reindexutxo
				pk := T_basic_Payload{}
				pk.cmd = "reindexutxo"
				s = fmt.Sprintf("%s", pk.cmd)

			case 19: //charge
				pk := T_Charge_Payload{}
				pk.cmd = "charge"
				pk.to_addr = "1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX"
				pk.amount = "100"
				s = fmt.Sprintf("%s -to %s -amount %s", pk.cmd, pk.to_addr, pk.amount)

			case 20: //send
				pk := T_Send_Payload{}
				pk.cmd = "send"
				pk.from_addr = "1F1tAaz5x1HUXrCNLbtMDqcw6o6GNn4xqT"
				pk.to_addr = "1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX"
				pk.amount = "100"
				s = fmt.Sprintf("%s -from %s -to %s -amount %s -mine", pk.cmd, pk.from_addr, pk.to_addr, pk.amount)

			case 21: //startnode
				pk := T_basic_Payload{}
				pk.cmd = "startnode"
				s = fmt.Sprintf("%s", pk.cmd)

			case 22: //getblock
				pk := T_GetBlock_Payload{}
				pk.cmd = "getblock"
				pk.blockid = "1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX"
				s = fmt.Sprintf("%s -blockid %s", pk.cmd, pk.blockid)
			}

			_, err := c.Write([]byte(s))
			checkError(err)

			if i++; i > 23 {
				i = 10
			}
			time.Sleep(1 * time.Second)

		} //for
	}(client)

	fmt.Scanln()

}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "[ERROR] %s", err.Error())
		//os.Exit(1)
	}
}
