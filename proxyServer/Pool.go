//-------------------------------------------------------
// Description: request pool
// Environment:
//
//
// kucst_2020.01.29
//-------------------------------------------------------
package main

import (
	"fmt"
	"strconv"

)

//-------------------------------------------------------
//    Requests { curLen, Request[] }
//    +--+--+--+--+--+
//    |  |  |  |  |<-|---Request
//    +--+--+--+--+--+
//-------------------------------------------------------
const POOL_LIMIT = 100

type Request struct {
	fromWho string //address
	reqCmd  string //request command
}

type Requests struct {
	requests []*Request
}

var gReqPool Requests

func NewRequest(fromWho, reqCmd string) *Request {
	defer logLeave("NewRequest()")
	req := Request{fromWho, reqCmd}
	return &req
}

func NewRequests() *Requests {
	defer logLeave("NewRequests()")
	reqs := Requests{}
	reqs.requests = make([]*Request, POOL_LIMIT)
	return &reqs
}

func (r *Requests) addRequest(req *Request) {
	defer logLeave("addRequest()")
	if len(r.requests)+1 >= POOL_LIMIT {
		r.delRequest(1)
	}
	r.requests = append(r.requests, req)
}

func (r *Requests) delRequest(overCount int) {
	defer logLeave("delRequest()")
	r.requests = append(r.requests[overCount:])
}

func (r *Requests) getLength() int {
	return len(r.requests)
}

func (r *Requests) printRequests() {
	for i, d := range r.requests {
		fmt.Println("index: ", string(strconv.Itoa(i)))
		fmt.Println("Request-fromWho: ", d.fromWho)
		fmt.Println("Request-reqCmd: ", d.reqCmd)
	}
}
